<?php
//this is the router
require_once('granular/prelude.php');

register_in_redis();

$q = explode('/',$_GET['q']);
$app = array_shift($q);
$class = array_shift($q);
$method = array_shift($q);
$args = $q;

if (!array_key_exists($app, $APPS)) {
    die();
}
$app = $APPS[$app];
if (!array_key_exists($class, $app)) {
    die();
}
$methods = $app[$class];

if (!in_array($method, $methods)) {
    die();
}

$obj = new $class;
$resp = call_user_func_array(array($obj, $method), $args);
rpc_respond($resp);
?>
