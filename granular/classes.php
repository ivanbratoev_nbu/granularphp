<?php

$APP_DIRS = array();

if ($handle = opendir(GRANULAR_APPS)) {

    while (false !== ($app = readdir($handle))) {
        if ($app!= "." && $app != "..") {
            array_push($APP_DIRS, $app);
        }
    }

    closedir($handle);
}

$APPS = array();

$_LOCAL = array();

foreach($APP_DIRS as $dir) {
    require_once(GRANULAR_APPS.$dir.'/setup.php');

    $_LOCAL[$dir] = $LOCAL;

    $before = get_declared_classes();
    if ($handle = opendir(GRANULAR_APPS.$dir.'/classes/')) {

        while (false !== ($class = readdir($handle))) {
            if ($class != "." && $class != "..") {
                require_once(GRANULAR_APPS.$dir.'/classes/'.$class);
            }
        }

        closedir($handle);
    }
    $after = get_declared_classes();
    $_classes = array_diff($after, $before);
    //limit to required
    $_classes = array_intersect($_classes, $LOCAL);
    $classes = array();
    foreach($_classes as $class) {
        $methods = array();
        foreach(get_class_methods($class) as $method) {
            if ($method != "init" && $method != "connect") {
                array_push($methods, $method);
            }
        }
        $classes[$class] = $methods;
    }
    $APPS[$dir] = $classes;
}

define('LOCAL', $APPS);

?>
