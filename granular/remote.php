<?php
abstract class Remote {

    public function __construct() {}

    public function connect() {
        $class = get_called_class();
        $reflector = new ReflectionClass($class);
        $app = basename(dirname(dirname($reflector->getFileName())));
        $methods = get_class_methods($class);
        return new RemoteInterface($app, $class, $methods);
    }
}

class RemoteInterface {

    var $__app;
    var $__class;
    var $__methods;

    public function __construct($app, $class, $methods) {
        unset($methods[array_search("connect", $methods)]);
        $this->__app = $app;
        $this->__class = $class;
        $this->__methods = $methods;
    }

    public function __call($method, $args) {
        if (in_array($method, $this->__methods)) {
            return rpc_call($this->__app, $this->__class, $method, $args);
        }
    }
}
?>
