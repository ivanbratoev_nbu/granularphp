<?php
function rpc_call($app, $class, $method, $args) {
    if (use_local($app, $class)) {
        $obj = new $class;
        return call_user_func_array(array($obj, $method), $args);
    } else {
        $url = get_from_redis($app, $class);
        $url = $url.$app."/".$class."/".$method."/";
        foreach ($args as $arg) {
            $url = $url.$arg."/";
        }
        $deflated = file_get_contents($url);
        $serialized = gzinflate($deflated);
        $obj = unserialize($serialized);
        return $obj;
    }
}

function rpc_respond($obj) {
    $serialized = serialize($obj);
    $deflated = gzdeflate($serialized);
    echo($deflated);
}

function use_local($app, $class) {
    if (array_key_exists($app, LOCAL)) {
        if (in_array($class,LOCAL[$app])) {
            return (sys_getloadavg()[0] < MAX_LOAD);
        }
    }
    return false;
}

?>
