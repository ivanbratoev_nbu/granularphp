<?php
function get_redis() {
    require_once("Predis/Autoloader.php");
    \Predis\Autoloader::register();
    try {
        return  new \Predis\Client(REDIS_CONNECTION);
    }
    catch (Exception $e) {
        die($e->getMessage());
    }
}

function get_hostname_and_ip() {
    $res = array();
    $res[0] = gethostname();
    $res[1] = gethostbyname(trim(exec("hostname")));
    return $res;
}

function get_from_redis($app, $class) {
    $key = $app."."."$class";
    $redis = get_redis();
    $url = $redis->srandmember($key);
    return $url."/granularphp/";
}

function register_in_redis() {
    $value = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'].":".$_SERVER['SERVER_PORT'];
    $redis = get_redis();
    foreach (LOCAL as $app => $classes) {
        foreach ($classes as $class => $methods) {
            $key = $app."."."$class";
            $redis->sadd($key, $value);
        }
    }
}

?>
